---
date: 2017-02-22T14:43:02-06:00
image: https://unsplash.it/300/200?blur&gravity=center
imageOverlayColor: "#000"
imageOverlayOpacity: 0.5
heroBackgroundColor: "#333"
title: About
description: "A little background on who I am."
---

Here's a little bit about me.
